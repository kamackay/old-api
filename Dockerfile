FROM node:lts-buster-slim as builder

WORKDIR /app

ADD package.json ./

RUN yarn install

ADD . .

CMD [ "yarn", "start" ]
